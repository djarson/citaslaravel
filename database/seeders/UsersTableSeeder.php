<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void

    {
        User::create([
            'name' => 'david',
            'email' => 'davidxona@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('12345678'),
            'dni'=>'4354689',
            'address' => 'los cireces',
            'mobile' => '964553355',
            'rol' =>'admin',
        ]);

        User::Factory()
            ->count(50)
            ->create();
    }
}
