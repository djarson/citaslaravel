<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Str;
 
$random = Str::random(8);

class DoctorController extends Controller
{
    public function index()
    {
        $doctors=User::doctors()->get();
        return view('doctors.index',compact('doctors'));
    }

    public function create()
    {
        return view('doctors.create');
    }

    public function store(Request $request)
    {
        $rules=[
            'name'=>'required|min:3',
            'email'=>'required|email',
            'dni'=>'required|digits:8',
            'address'=>'nullable|min:6',
            'mobile'=>'required|min:9',

        ];
        $messages=[
            'name.required'=>'el nombre del medico es obligatorio',
        ];
        $this->validate($request,$rules,$messages);

        User::create(
            $request->only('name','email','dni','address','mobile')+
            [
                'rol'=>'doctor',
                'password'=>bcrypt($request->input('password')),
            ]
            );
            $notificacion='el medico se ha registradi correctamente';
            return redirect('/medicos')->with(compact('notificacion'));
    }

  
    public function show(string $id)
    {
        
    }

   
    public function edit(string $id)
    {
        
    }

    public function update(Request $request, string $id)
    {
        
    }

    public function destroy(string $id)
    {
        
    }
}
