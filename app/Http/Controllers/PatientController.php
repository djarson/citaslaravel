<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;


class PatientController extends Controller
{
    
    public function index()
    {
        $patients=User::patients()->get();
        return view('patients.index',compact('patients'));
    }

    
    public function create()
    {
        return view('patients.create');
    }

   
    public function store(Request $request)
    {

    }


    public function show(string $id)
    {
        
    }

 
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
